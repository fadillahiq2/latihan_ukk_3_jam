<?php

use App\Http\Controllers\DaftarController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('landing');

Route::get('/daftar', [DaftarController::class, 'siswa_daftar'])->name('daftar');
Route::post('/daftarPost', [DaftarController::class, 'siswa_daftar_post'])->name('daftarPost');
Route::get('/daftar/print/{nis}', [DaftarController::class, 'siswa_print'])->name('daftarPrint');



Auth::routes([
    'register' => false,
    'verify' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/edit/{nis}', [HomeController::class, 'siswa_edit'])->name('daftarEdit');
Route::put('/home/editUpdate/{nis}', [HomeController::class, 'siswa_update'])->name('daftarUpdate');
Route::get('/home/verif', [HomeController::class, 'siswa_verif'])->name('daftarVerif');
Route::post('/home/verifPost', [HomeController::class, 'siswa_verif_post'])->name('daftarVerifPost');
Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->middleware('is_admin')->name('adminHome');
Route::delete('/adminDelete/{nis}', [App\Http\Controllers\HomeController::class, 'admin_delete'])->middleware('is_admin')->name('adminDelete');
