<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\SiswaVerif;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::with('siswa')->get();
        return view('home', compact('user'));
    }

    public function admin(){
        $siswas = SiswaVerif::latest()->paginate(10);
        return view('adminHome', compact('siswas'))->with('i');
    }

    public function siswa_edit($nis){
        $siswa = Siswa::findOrFail($nis);
        return view('siswa.edit', compact('siswa'));
    }

    public function siswa_verif(){
        $siswa = User::with('siswa')->get();
        return view('siswa.verif', compact('siswa'));
    }

    public function siswa_update(Request $request, $nis){
        $request->validate([
            'nama' => 'required',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);

        $siswa = Siswa::findOrFail($nis);

        $siswa->update($request->all());

        return redirect()->route('home')->with('success', 'Data berhasil diperbarui');
    }

    public function siswa_verif_post(Request $request){
        $check = DB::table('siswa_verifs')->where('nis', $request->nis)->first();
        $request->validate([
            'nis' => 'required',
            'email' => 'required',
            'nama' => 'required',
            'jenkel' => 'required',
            'temp_lahir' => 'required',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);

        if(!$check){
            SiswaVerif::create($request->all());

            return redirect()->route('daftarVerif')->with('success', 'Data berhasil diverifikasi !');
        }else if($check){
            return redirect()->route('daftarVerif')->with('error', 'Data sudah pernah diverifikasi !!!');
        }
    }

    public function admin_delete($nis){
        $siswa = SiswaVerif::findOrFail($nis);
        $siswa->delete();

        return redirect()->route('adminHome')->with('success', 'Data berhasil dihapus!');
    }
}
