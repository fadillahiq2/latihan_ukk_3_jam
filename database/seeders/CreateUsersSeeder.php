<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('Market22'),
                'remember_token' => Str::random(60),
                'is_admin' => 1,
            ],
        ];


        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
